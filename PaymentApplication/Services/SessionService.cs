﻿namespace PaymentApplication.Services
{
    using System;
    using System.Linq;

    using PaymentApplication.Entities;
    using PaymentApplication.Infrastructure;

    /// <inheritdoc />
    public class SessionService : ISessionService
    {
        /// <inheritdoc />
        public void Add(Session session)
        {
            using (var dbContext = new SqliteDbContext())
            {
                dbContext.Database.EnsureCreated();
                dbContext.Session.Add(session);
                dbContext.SaveChanges();
            }
        }

        /// <inheritdoc />
        public bool IsValid(Guid sessionId)
        {
            Session session = null;
            using (var dbContext = new SqliteDbContext())
            {
                dbContext.Database.EnsureCreated();
                session = dbContext
                    .Session
                    .AsEnumerable()
                    .FirstOrDefault(
                    s =>
                        s.Id == sessionId
                        && DateTime.UtcNow <= s.Expires);
            }

            return session != null;
        }
    }
}