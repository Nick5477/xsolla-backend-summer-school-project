﻿namespace PaymentApplication.Controllers
{
    using System;
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Mvc;

    using PaymentApplication.Entities;
    using PaymentApplication.Models;
    using PaymentApplication.Services;

    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly ISessionService _sessionService;

        private readonly IPaymentCheckingService _paymentCheckingService;

        public PaymentController(
            ISessionService sessionService,
            IPaymentCheckingService _paymentCheckingService)
        {
            _sessionService = sessionService;
            this._paymentCheckingService = _paymentCheckingService;
        }

        /// <summary>
        /// Создает платежную сессию.
        /// </summary>
        /// <returns>Идентификатор платежной сессии.</returns>
        [HttpGet("[action]")]
        public Dictionary<string, string> Create()
        {
            var newSession = new Session(Guid.NewGuid(), DateTime.UtcNow.AddMinutes(15));
            _sessionService.Add(newSession);

            return new Dictionary<string, string>()
            {
                { "sessionId", newSession.Id.ToString() }
            };
        }

        /// <summary>
        /// Метод оплаты.
        /// </summary>
        /// <param name="paymentRequestModel">Передаваемые данные карты и платежной сессии.</param>
        /// <returns>Ответ с кодом 200 в случае успеха и ответ с кодом 404 в случае ошибки.</returns>
        [HttpPost("[action]")]
        public ActionResult Pay([FromBody] PaymentRequestModel paymentRequestModel)
        {
            if (!Guid.TryParse(paymentRequestModel.SessionId, out var res)
                || !_sessionService.IsValid(res))
            {
                return NotFound("Указан невалидный идентификатор сессии.");
            }

            try
            {
                if (_paymentCheckingService.IsValidCard(
                    paymentRequestModel.CardNumber,
                    paymentRequestModel.CardExpirationDate,
                    paymentRequestModel.CardCVC))
                {
                    return Ok("Оплата завершена успешно.");
                }
            }
            catch (ArgumentException e)
            {
                // ignored
            }

            return NotFound("Оплата не прошла. Некорректная карта.");
        }
    }
}