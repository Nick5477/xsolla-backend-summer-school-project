﻿namespace PaymentApplication.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Session
    {
        [Key]
        public Guid Id { get; }

        public DateTime Expires { get; }

        public Session()
        {
        }

        public Session(Guid id, DateTime expires)
        {
            Id = id;
            Expires = expires;
        }
    }
}