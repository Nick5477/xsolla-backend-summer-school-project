# Payment application for Xsolla Summer Backend School
Разработанное приложение представляет собой платежную систему, которая имитирует процесс оплаты банковской картой.

## OpenAPI спецификация
[OpenAPI-спецификация для данного приложения.](https://app.swaggerhub.com/apis-docs/nick5477/payment-application_api/v1 "OpenAPI-спецификация для данного приложения")